const skygear = require("skygear");

skygear.config({
    'endPoint': 'XXX', // trailing slash is required
    'apiKey': 'XXX',
}).then(() => {
    console.log('skygear container is now ready for making API calls.');
}, (error) => {
    console.error(error);
});


module.exports = skygear;